# gitlab-alfred-rb

Handy shortcuts for GitLab.

## Examples

Below are a few example use cases, but this list is not exhaustive. This project
attempts to stick to the "language" of GitLab by mimicking its [GitLab Flavored
Markdown][gfm] syntax (e.g., `!` for merge requests, `#` for issues, etc.) while
making it a bit more context-aware (e.g., `mine` for things authored by you,
`ass[igned]` for things assigned to you).

[gfm]: https://docs.gitlab.com/ee/user/markdown.html

### View your own profile

```shell
$ bin/gitlab-alfred 'me'
https://gitlab.com/rspeicher
```

### View your todos

```shell
$ bin/gitlab-alfred 'todo'
https://gitlab.com/dashboard/todos/
```

### View merge requests assigned to you

```shell
$ bin/gitlab-alfred '!ass'
https://gitlab.com/dashboard/merge_requests/?assignee_username=rspeicher
```

### View a project

```shell
$ bin/gitlab-alfred 'fw'
https://gitlab.com/gitlab-org/release/framework
```

See [`./config/default.yml.example`](./config/default.yml.example) for examples
of assigning projects to shortened names.

### View issues you created in a project

```shell
$ bin/gitlab-alfred 'ce #mine'
https://gitlab.com/gitlab-org/gitlab-ce/issues?author_username=rspeicher
```

### Create a new issue in a project

```shell
$ bin/gitlab-alfred 'ee #new'
https://gitlab.com/gitlab-org/gitlab-ee/issues/new
```

### View commits in a project's default branch

```shell
$ bin/gitlab-alfred 'ob @'
https://gitlab.com/gitlab-org/omnibus-gitlab/commits/master
```

### View a specific commit in a project

```shell
$ bin/gitlab-alfred 'rt @b1af513a'
https://gitlab.com/gitlab-org/release-tools/commit/b1af513a
```

## Integration

Wire the script up to an [Alfred](https://www.alfredapp.com/) workflow, and you
can have it open the resulting URL. :sparkles: :rocket:

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
