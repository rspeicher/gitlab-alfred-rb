require 'spec_helper'

describe 'bin/gitlab-alfred' do
  def run(*args)
    system('bin/gitlab-alfred', *args)
  end

  it 'supports `#mine`' do
    expect { run('#mine') }
      .to output("https://gitlab.com/dashboard/issues/?author_username=liz.lemon\n")
      .to_stdout_from_any_process
  end

  it 'supports `!assigned`' do
    expect { run('!assigned') }
      .to output("https://gitlab.com/dashboard/merge_requests/?assignee_username=liz.lemon\n")
      .to_stdout_from_any_process
  end

  it 'supports `[project] #new`' do
    expect { run('ce', '#new') }
      .to output("https://gitlab.com/gitlab-org/gitlab-ce/issues/new\n")
      .to_stdout_from_any_process
  end
end
