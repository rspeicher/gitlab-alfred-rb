require 'spec_helper'

require 'configuration'

describe Configuration do
  describe '.project' do
    it 'finds a project by name' do
      expect(described_class.project('ce').path)
        .to eq 'https://gitlab.com/gitlab-org/gitlab-ce'
    end

    it 'finds a project with multiple names' do
      expect(described_class.project('infra').path)
        .to eq 'https://gitlab.com/gitlab-com/gl-infra/infrastructure'
    end

    it 'raises ProjectNotFoundError when no project is found' do
      expect { described_class.project('foo') }
        .to raise_error(described_class::ProjectNotFoundError)
    end
  end
end
