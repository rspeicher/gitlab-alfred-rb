require 'configuration_file'

class Configuration
  ProjectNotFoundError = Class.new(StandardError)
  Project = Struct.new(:path, :names)

  def self.parse
    @config ||= ConfigurationFile.new.parse
  end

  def self.base
    parse['base']
  end

  def self.username
    parse['username']
  end

  def self.projects
    @projects ||= parse['projects'].map do |entry|
      Project.new(entry['path'], Array(entry['names']))
    end
  end

  def self.project(name)
    projects.detect(-> { raise ProjectNotFoundError }) do |project|
      project.names.include?(name)
    end
  end

  def self.project_names
    projects.flat_map(&:names)
  end
end
