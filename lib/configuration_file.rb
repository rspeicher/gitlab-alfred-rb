# frozen_string_literal: true

require 'yaml'

class ConfigurationFile
  USER_PATH = File.expand_path('../config/default.yml', __dir__)
  EXAMPLE_PATH = "#{USER_PATH}.example"

  class NotFoundError < StandardError
    def initialize(path)
      super "Expected configuration at #{path}"
    end
  end

  def self.detect_configuration
    if ENV['TEST']
      EXAMPLE_PATH
    else
      [USER_PATH, EXAMPLE_PATH].detect { |f| File.exist?(f) }
    end
  end

  attr_reader :path

  def initialize
    @path = self.class.detect_configuration

    validate_path!
  end

  def parse
    yaml_string = File.read(path)

    YAML.safe_load(yaml_string, [], [], true, path)
  rescue Psych::Exception => ex
  end

  private

  def validate_path!
    return if path

    raise NotFoundError.new(path)
  end
end
