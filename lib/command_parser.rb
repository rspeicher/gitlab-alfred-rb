require 'configuration'

class CommandParser
  def self.names_pattern
    # TODO (rspeicher): Regexp.escape names?
    Regexp.new(Configuration.project_names.join('|'))
  end

  def initialize(input)
    @command = Array(input).join(' ').downcase
  end

  DASHBOARD_PATTERN = %r{
    \A
    (
      (?<type>\#|\!)
      (?<id>\w+)
      |
      (?<type>todos?)
    )
    \z
  }x.freeze

  PROJECT_PATTERN = %r{
    \A
    (?<project>#{self.names_pattern})
    (\s+
      (?:
        (?<type>\#|\!|)
        (?<id>\w+)?
      |
        (?<type>\@)
        (?<id>.*)?
      )
    )?
    \z
  }x.freeze

  def parse
    if @command == 'me'
      "#{Configuration.base}/#{Configuration.username}"
    elsif DASHBOARD_PATTERN.match(@command)
      dashboard_url($~)
    elsif PROJECT_PATTERN.match(@command)
      project_url($~)
    end
  end

  private

  def dashboard_url(match_data)
    "#{Configuration.base}/dashboard/".tap do |url|
      case match_data[:type]
      when '#'
        url << 'issues/'
      when '!'
        url << 'merge_requests/'
      when 'todo', 'todos'
        url << 'todos/'
      end

      url << issuable_url(match_data[:id])
    end
  end

  def project_url(match_data)
    project = Configuration.project(match_data[:project])

    project.path.dup.tap do |url|
      case match_data[:type]
      when '#'
        url << '/issues'
        url << issuable_url(match_data[:id])
      when '!'
        url << '/merge_requests'
        url << issuable_url(match_data[:id])
      when '@'
        url << commit_url(match_data[:id])
      end
    end
  end

  def issuable_url(issuable_id)
    return '' unless issuable_id

    if issuable_id.start_with?('ass')
      "?assignee_username=#{Configuration.username}"
    elsif issuable_id == 'mine'
      "?author_username=#{Configuration.username}"
    elsif issuable_id == 'todo'
      "?assignee_username=#{Configuration.username}&not[author_username]=#{Configuration.username}"
    else
      "/#{issuable_id}"
    end
  end

  def commit_url(id)
    return '' if id.nil?

    id.strip!

    if id.empty?
      '/commits/master'
    elsif id.match?(/^\h{6,40}$/)
      # View a specific commit if it looks like a SHA
      "/commit/#{id}"
    else
      # View a named branch
      "/commits/#{id}"
    end
  end
end
